﻿using System;
using System.IO;

namespace Snoop.Sql2Lambda.Basic
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * En este primer módulo vamos a aprender lo básico del trabajo con Lambdas.
             * Lambdas en .NET son una herramienta muy útil a la hora de trabajar con listas 
             * ya que nos permite filtrarlas, modificarlas, ubicar un elemento, saber si existe
             * un elemento que cumpla determinada condición, etc.
             * Su gran utilidad recae luego en que podremos trabajar con tablas de SQL como si fuesen listas de .NET,
             * mediante Entity Framework, entonces si sabemos como trabajar con listas, podremos interactuar con nuestra DB 
             * sin escribir nada de SQL.
             * Pero esto es tema de más adelante, por ahora empecemos por el inicio.
             * Qué es un lambda? Podemos definirlo como una 'función de una linea'. Es decir, es definimos una entrada,
             * un procesamiento y una salida. Vamos a un caso concreto.
             */

            // Nunca hay que perder de vista que un Lambda es una función.
            // Si buscamos análogos, podemos decir que una función definida de tal manera:
            int CalcularCuadrado(int numero) 
            {
                return numero * numero;
            }
            CalcularCuadrado(2);

            // Podemos escribirla mediante la sintaxis de lambda de la siguiente manera:
            Func<int, int> CalcularCuadradoLambda = (numero) => numero * numero;
            CalcularCuadradoLambda(2);

            // Vamos a explicar un poquito esto:
            // |> 'Func<int, int>': es el 'tipo de dato' (como string, o int) al cual asignamos un lambda. 
            //      No vamos a entrar mucho en detalle de esto, porque es algo más avanzado y complejo, 
            //      únicamente lo mencionamos por ahora
            // |> 'CalcularCuadradoLambda': es el nombre del 'método'
            // |> '(numero)': va a ser el/los parámetros de nuestro Lambda (sería como 'int numero').
            // |> '=>': es la sintaxis propio de Lambdas. SIEMPRE se usa esta sintaxis cuando hablamos de lambda.
            // |> 'numero * numero': es lo que va a hacer nuestro Lambda, el procesamiento que definimos. 

            /* Algunas posibles preguntas: 
             * ¿Cómo sabe lambda que el ingreso va a ser un int y la salida un int? Esto lo sabe porque 
             *      definimos la variable como Func<int, int>. En este caso, la defimos como 'una función
             *      que recibe un int y devuelve un int', entonces el parámetro va a ser un int y la salida TIENE
             *      que ser un int. Si modificamos la sintaxis y ponemos '... => "un texto"', obtendremos un error 
             *      ya que "un texto" no es un int válido
             *  ¿Van parentesis? ¿Llaves? ¿'return'? Este comportamiento es similar al de js. 
             *      Si tenemos un solo parámetro, no es necesario colocar los partensis. En nuestro ejemplo, podríamos poner 
             *      'numero => numero * numero' y sería válido. Si tenemos más de un parámetro, SI es obligatorio tenerlo.
             *      Si el lambda es una sola linea, no es necesario colocar llaves ni 'return'. 
             *      Si necesitamos escribir más de una linea en un lambda, deberemos colocar llaves y 'return':
             *      (numero) => { return numero * numero; }
             *      
             *  La idea de un Lambda no es hacer una función de muchas lineas, ya que puede resultar medio complejo
             *      y dificil de modificar luego. En la teoría deberían ser lógicas cortas, ya veremos ejemplos.
             */

            // Vamos a trabajar un poco para afianzar estos conocimientos.
            // Traduzcamos estos métodos a Lambda. Abajo tenes las respuestas para validar, 
            // te damos ya como debería estar definida la Func para no marearte con eso.

            string EliminarEspacios(string texto)
            {
                return texto.Trim();
            }
            // Func<string, string> EliminarEspaciosLambdas = ....

            int CalcularDoble(int numero)
            {
                return numero * 2;
            }
            // Func<int, int> CalcularDobleLambdas = ....

            string ObtenerProductoComoString(int numero1, int numero2) 
            {
                return (numero1 * numero2).ToString();
            }
            // Func<int, int, string> ObtenerProductoComoStringLambda = ...

            int ObtenerUno() 
            {
                return 1;
            }
            //Func<int> ObtenerUnoLambda = ...

            bool EsPositivo(int numero)
            {
                return numero > 0;
            }
            //Func<int, bool> EsPositivoLambdas = ....











            #region Respuestas
            #region EliminarEspacios
            Func<string, string> EliminarEspaciosLambdasSolucion = (texto) => texto.Trim();
            #endregion
            #region CalcularDoble
            Func<int, int> CalcularDobleLambdasSolucion = (numero) => numero * 2;
            #endregion
            #region ObtenerProductoComoString
            Func<int, int, string> ObtenerProductoComoStringLambdaSolucion = (numero1, numero2) => (numero1 * numero2).ToString();
            #endregion
            #region ObtenerUno
            Func<int> ObtenerUnoLambdaSolucion = () => 1;
            #endregion
            #region EsPositivo
            Func<int, bool> EsPositivoLambdasSolucion = (numero) => numero > 0;
            #endregion
            #endregion

            /*
             * Muy bien! Ya sabes como definir lambdas, en el próximo módulo vamos
             * a arrancar con cosas un poco más complejas, así que es importante tener
             * esta base sólida. Para seguir, anda al proyecto Snoop.Sql2Lambda.Modulo1
             */
        }
    }
}
