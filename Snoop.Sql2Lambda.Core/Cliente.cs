﻿namespace Snoop.Sql2Lambda.Core
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public long Cuil { get; set; }
        public string Direccion { get; set; }
        public Provincia Provincia { get; set; } 
        public long Telefono { get; set; }
        public bool EsPremium { get; set; }
    }
}
