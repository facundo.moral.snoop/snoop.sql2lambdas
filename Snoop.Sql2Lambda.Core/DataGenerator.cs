﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Snoop.Sql2Lambda.Core
{
    public static class DataGenerator
    {
        public static long GetNumeroFalso(int longitud)
        {
            var rand = new Random();
            string result = "";
            for (int i = 0; i < longitud; i++)
            {
                result += rand.Next(10);
            }
            return long.Parse(result);
        }

        public static List<Producto> GetProductos()
        {
            string separador = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var rand = new Random();
            var result = new List<Producto>();

            var cantidadRegistros = rand.Next(30);

            for (int i = 1; i <= cantidadRegistros; i++)
            {
                var Producto = new Producto
                {
                    Descripcion = $"Descripcion del producto {i}",
                    IdProducto = i,
                    Categoria = rand.Next(10),
                    PesoPorUnidad = decimal.Parse(GetNumeroFalso(5)+ separador + GetNumeroFalso(4))
                };
                result.Add(Producto);
            }
            return result;
        }

        public static List<Cliente> GetClientes()
        {
            var rand = new Random();
            var result = new List<Cliente>();

            var cantidadRegistros = rand.Next(30);

            Array provincias = Enum.GetValues(typeof(Provincia));

            for (int i = 1; i <= cantidadRegistros; i++)
            {
                var cliente = new Cliente
                {
                    IdCliente = i,
                    Cuil = GetNumeroFalso(10),
                    Direccion = $"Calle Falsa {rand.Next(500)}",
                    EsPremium = rand.Next(1) == 0,
                    Telefono = GetNumeroFalso(10),
                    Provincia = (Provincia)provincias.GetValue(rand.Next(provincias.Length))
                };
                result.Add(cliente);
            }
            return result;
        }


    }
}
