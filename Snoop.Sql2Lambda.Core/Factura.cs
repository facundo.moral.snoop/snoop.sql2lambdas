﻿using System;
using System.Collections.Generic;

namespace Snoop.Sql2Lambda.Core
{
    public class Factura
    {
        public int IdFactura { get; set; }
        public DateTime Fecha { get; set; }
        public Cliente Cliente { get; set; } = new Cliente();
        public List<Producto> Productos { get; set; } = new List<Producto>();
    }
}
