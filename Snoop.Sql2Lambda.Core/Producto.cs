﻿namespace Snoop.Sql2Lambda.Core
{
    public class Producto
    {
        public int IdProducto { get; set; }
        public string Descripcion { get; set; }
        public decimal PesoPorUnidad { get; set; }
        public int Categoria { get; set; }
    }
}
