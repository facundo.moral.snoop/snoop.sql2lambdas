﻿namespace Snoop.Sql2Lambda.Core
{
    public enum Provincia
    {
        BUENOS_AIRES,
        CORDOBA,
        SANTA_FE,
        LA_PAMPA,
        MISIONES,
        ENTRE_RIOS,
        CORRIENTES,
        CHACO,
        CHUBUT
    }
}
