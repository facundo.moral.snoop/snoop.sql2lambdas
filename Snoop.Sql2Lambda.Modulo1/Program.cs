﻿using Snoop.Sql2Lambda.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Snoop.Sql2Lambda.Modulo1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Para esta altura suponemos que ya sabes que es un lambdas, y sabes como 
             * definirlos. Vamos ahora a ver cómo se aplican al manejo de lista, algo muy útil.
             * A lo largo de todo este pequeño curso vamos a utilizar el mismo modelo 
             * de datos para no marearnos, será un modelo muy simple de Facturacion:
             * Cliente, Factura, Producto.
             * 
             * Como previo a todo, vamos a aclarar que todos los métodos de listas
             * (.Where, .Select, .Any, entre otros) reciben, en su gran mayoria, lambdas.
             * Dichos lambdas son funciones a aplicar sobre cada item de la lista, y según
             * el tipo de método es que deberemos definir el lambda para que devuelva un determinado 
             * tipo de dato: bool, un string, un int, etc. Luego, obtendremos el resultado de aplicar el 
             * lambda a cada item. Por ejemplo, si queremos filtrar una lista, deberemos
             * indicar un lambda cuyo ingreso sea un item de la lista y la salida sea un bool indicando
             * si el item en cuestión satisface la condición del filtro.
             * Empecemos con un ejemplo:
             */


            // Dada una lista de clientes:
            var clientes = DataGenerator.GetClientes();

            // Obtener el cliente con Id 1 y asignar a la siguiente variable:
            Cliente clienteEncontrado;

            // Si no supieramos de la existencia de lambdas, cómo haríamos esto?
            // Supongo se te vino a la cabeza algo como esto:
            foreach (var cliente in clientes)
            {
                if (cliente.IdCliente == 1)
                {
                    clienteEncontrado = cliente;
                }
            }
            // O con un bucle FOR capaz. Ambas soluciones son totalmente válidas, pero veamos como los lambdas
            // nos hacen el código más simple.

            // Vamos a utilizar el método .First, que busca en una lista y devuelve el
            // el primer item que coincida con una condición. Ahora, cómo le indicamos la condición que debe 
            // cumplir un registro? Con un lambda, cuya entrada será un cliente y su salida un bool:
            clienteEncontrado = clientes.First((cliente) => cliente.IdCliente == 1);

            // Acá vemos la sintaxis de lambdas que vimos en el anterior módulo, ya deberías reconocerla.
            // Además, vemos que el input es un cliente '(cliente)' y su salida un bool 'cliente.IdCliente == 1'

            // Es evidente que el código es más simple, y más aún si lo hacemos un poquito más corto simplificando 
            // algunas cosas:
            clienteEncontrado = clientes.First(c => c.IdCliente == 1);

            // Generalmente, el parámetro del lambda es una variable de una sola letra o dos, y se suele colocar la 
            // primer letra de la entidad en cuestión, en este caso la 'c' de Cliente

            // 1) Escribí un lambda que busque un cliente cuya Provincia sea Provincia.BUENOS_AIRES
            // clienteEncontrado = ?;

            // Perfecto, ya entedes como funciona el First. Vamos a agregar otro método que es muy similar pero 
            // con una pequeña diferencia: FirstOrDefault. Capaz te preguntaste que pasa que no encuentra ningun cliente
            // que cumpla la condición.. muy bien, en ese caso el método .First arrojará un error. Hay veces que esto no es 
            // lo deseable; para estos casos existe FirstOrDefault. Su funcionamiento es igual al .First, únicamente que este
            // devolverá null en caso que no encuentre nada. Luego deberemos nosotros validar si el cliente no es null. Por ejemplo:
            clienteEncontrado = clientes.FirstOrDefault(c => c.IdCliente == 1);
            if (clienteEncontrado != null) 
                Console.WriteLine("Encontramos el usuario!");
            else 
                Console.WriteLine("No encontramos el usuario :(");


            // 2) Replicar el ejercicio 1 pero utilizando FirstOrDefault
            // clienteEncontrado = ?;

            // ACLARACION: tanto el First como el FirstOrDefault tiene una sobrecarga que no recibe ningun
            // parametro y devuelve el primer elemento de la lista. No se utilian lamdas por lo que no lo
            // agregamos al curso, pero está bueno mencionarlo.

            // Perfecto, de a poco vamos sumando métodos a nuestro repetorio. Vamos con otro muy útil también:
            // .Where. Dicho método funciona de manera similar al First(OrDefault) pero en este caso, la salida
            // serán TODOS los registros que cumplan la condición. Por ejemplo, para buscar todos los usuarios 
            // con ID  > 5:

            // Primero debemos definir una variable para el resultado que ya no será de tipo Cliente sino una LISTA de clientes
            List<Cliente> clientesEncontrados;

            clientesEncontrados = clientes.Where(c => c.IdCliente > 5).ToList();
            // La sintaxis es muy similar a la que ya vimos, no vamos a entrar mucho en detalle.
            // Únicamente mencionamos una pequeña diferencia: .ToList 
            // En c#, tenemos varias clases para almacenar conjuntos: 
            // * arrays ([])
            // * Enumarable (Enumerable)
            // * List (List)
            // Entre otras. No vamos a entrar en detalle de las diferencias porque son pocas, generalmente
            // más asociadas a las perfomance. En este curso vamos a usar siempre listas, que son las más "completas",
            // es decir, muchos de los métodos que vamos a ver se aplican únicamente sobre listas. 
            // Sin embargo, la salida de muchos métodos (como el .Where) que vimos recién, es un Enumarable. 
            // En este caso casteamos simplemente con un .ToList.
            // ACLARACION: generalmente hay relación jerarquica entre las clases de listas. Por ejemplo,
            // List hereda de Enumarable, por lo que muchas veces un casteo no es necesario.

            // 3) Escribir un lambda para que busque todos los usuarios cuyo teléfono comience con 11.
            // Pista: podes usar el método string.StartsWith, pero el Telefono es de tipo Long, vas a tener que caster
            // este dato. No olvides el .ToList!

            // clientesEncontrados = ?

            // 4) Escribir un lambda para que busque todos los usuarios Premium (.EsPremium)
            // clientesEncontrados = ?

            // Venimos joya hasta acá. Vamos a ver el último método de este módulo: el .Select
            // El select, como era de esperarse, también recibe un lambda. Pero, a diferencia de los
            // anterior, no filtra la lista, por lo que la salida de nuestro lambda NO tiene que ser un bool.
            // Qué hace el .Select entonces? Simplemente "selecciona" de la lista los elementos que digamos,  
            // y nos devuelve una nueva lista con esos campos que seleccionamos. Vamos a ver un ejemplo
            // asi queda más claro:

            // Si quiero seleccionar todos los teléfono de los clientes:
            List<long> telefonos = clientes.Select(c => c.Telefono).ToList();
            // Qué estoy haciendo? A cada cliente le aplico el lambda (c => c.Telefono). Dicho lambda recibe un cliente
            // y devuelve su teléfono. Entonces, mi salida va a ser una lista de teléfono, uno por cada cliente.

            // Vamos a escribir un par así vemos más ejemplos
            List<string> direcciones = clientes.Select(c => c.Direccion).ToList();
            List<int> ids = clientes.Select(c => c.IdCliente).ToList();

            // Perfecto, bastante simple, vamos a complejizarlo un poco
            // 5) Escribir un lambda para que devuelva una lista de string que va a estar conformada por "{Direccion} - {Telefono}".
            //List<string> direccionesTelefonos = ?

            // Antes de seguir con más ejercicio integradores, vamos a aclarar que los métodos estos son
            // anidables. Es decir, podemos a la salida de un método, aplicarle otro lambda: .Where(...).Select(...)

            // 6) Escribir un lambda que obtenga todos los IDs de los clientes premium
            // List<int> idClientesPremium = ?

            // 7) Escribir un lambda que obtenga la lista de teléfonos de todos los usuarios de BUENOS_AIRES
            // pero agregando previamente "(011)"
            // List<string> telefonosBuenosAires = ?

            // Vamos a anexar luego una lista de más ejercicios, para que puedas practicar bien estas cosas.
            // Por ahora, vamos a continuar con el módulo 2 así seguimos incorporando más métodos!

            #region (1)
            clienteEncontrado = clientes.First(c => c.Provincia == Provincia.BUENOS_AIRES);
            #endregion
            #region (2)
            clienteEncontrado = clientes.FirstOrDefault(c => c.Provincia == Provincia.BUENOS_AIRES);
            #endregion
            #region (3)
            clientesEncontrados = clientes.Where(c => c.Telefono.ToString().StartsWith("11")).ToList();
            #endregion
            #region (4)
            clientesEncontrados = clientes.Where(c => c.EsPremium).ToList();
            #endregion
            #region (5)
            List<string> direccionesTelefonosSolucion = clientes.Select(c => c.Direccion + " - " + c.Telefono).ToList();
            #endregion
            #region (6)
            List<int> idClientesPremiumSolucion = clientes.Where(c => c.EsPremium).Select(c => c.IdCliente).ToList();
            #endregion
            #region (7)
            List<string> telefonosBuenosAiresSolucion = clientes.Where(c => c.Provincia == Provincia.BUENOS_AIRES).Select(c => "(011)" + c.Telefono).ToList();
            #endregion
        }
    }
}
