﻿using Snoop.Sql2Lambda.Core;
using System;
using System.Linq;

namespace Snoop.Sql2Lambdas.Modulo2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             * Ya vimos los métodos más  usados para manejar listas. Vamos a agregar un par más,
             * aunque vamos a subir un poco la velocidad, ya sabes lo que es un lambda
             * y lo que significan
             */

            var productos = DataGenerator.GetProductos();  // Datasets para pruebas
            var clientes = DataGenerator.GetClientes();

            // Vamos a ver al "primo" del .First: el .Single
            // Recibe el mismo parámetro que el First, y devuelve lo mismo, pero arrojará una exception
            // si no existe un ÚNICO registro que cumpla la condición.

            // Por ejemplo, si tenemos dos usuarios premium, el siguiente código arrojará
            // una exception:
            clientes.Single(c => c.EsPremium); // (!)
            // No vamos a hacer práctica de este método, es muy similar a los que ya vimos.

            // Vamos ahora a ver 3 métodos que trabajan con números: Min, Max y Sum
            // Min y Max, como indican sus nombres, nos devuelven de una lista el mayor o menor
            // numero segun un lambda que indicamos. Como pueden suponer, dicho lambda tiene que tener 
            // como salida un numero, que será sobre el cual se aplicará el Max o Min.
            // Por ejemplo
            productos.Min(p => p.PesoPorUnidad);
            // Obtiene el producto más liviano

            // Como habran notado el lambda definido (p => p.PesoPorUnidad) tiene como salida un numero, 
            // el peso en este caso
            // Similar, pero con Max, para obtener el más pesado
            productos.Max(p => p.PesoPorUnidad);

            // Luego, tenemos uno que recibe un lambda de formato similar: Sum
            // En este caso, suma una lista de numeros. Por ejemplo, si queremos obtener la sumatoria
            // de todos los pesos:
            productos.Sum(p => p.PesoPorUnidad);

            // No vamos a ahondar mucho en detalle de estos tres métodos, son simples, vamos con un par de 
            // ejercicios y seguimos:

            // 1) Definir un lambda que obtenga el ID de producto más alto

            // 2) Definir un lambda que obtenga la dirección más corta, basandonos en el número de caracteres
            // del campo Cliente.Direccion. Pista: usa string.Length

            // 3) Obtener la suma de todos pesos de productos con categoria 1. Acordate que podes anidar métodos!

            // 3) bis
            // Seguramente planteaste el ejercicio con un .Where y un .Sum. Perfecto en ese caso.
            // Pero, a modo "juego mental", te propongo hacerlo solamente un Sum. Como lo harías?


            #region Soluciones
            #region (1)
            productos.Max(p => p.IdProducto);
            #endregion
            #region (2)
            clientes.Min(c => c.Direccion.Length);
            #endregion
            #region (3)
            productos.Where(p => p.Categoria == 1).Sum(p => p.PesoPorUnidad);
            #endregion
            #region (3) bis
            productos.Sum(p => p.Categoria == 1 ? p.Categoria : 0);
            #endregion
            #endregion
        }
    }
}
