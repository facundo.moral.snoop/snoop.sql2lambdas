﻿using Snoop.Sql2Lambda.Core;
using System;
using System.Linq;

namespace Snoop.Sql2Lambdas.Modulo3
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Perfecto, ya tenemos una buena idea de como manejar listas.
             * Vamos a seguir agregando métodos a nuestro repertorio, en 
             * este caso métodos para filtrar listas.
             */
            var clientes = DataGenerator.GetClientes();

            // .All: este método nos sirve para saber si TODOS los items
            // de una lista cumplen determinada condición. Como ya 
            // podrías suponer para esta altura, el parámetro va a ser 
            // un lambda cuya salida sea un booleano, si la salida de 
            // evaluar el lambda para TODOS los registros es TRUE,
            // el método devolverá TRUE. Por ejemplo, si quisiesemos 
            // saber si todos los clientes son premium podrías hacer así:
            clientes.All(c => c.EsPremium);

            // .Any: es similar al método anteriormente mencionado, 
            // con la diferencia que devolverá TRUE si alguno de los registros
            // cumple la condición especificada. Por ejemplo, siguiendo el
            // ejemplo anterior:
            clientes.Any(c => c.EsPremium);
            // devolverá TRUE con que aunque sea UNO de nustros clientes sea PREMIUM

            // Sigamos con otros dos métodos muy utiles: Skip y Take
            // Como el nombre nos puede hacer suponer, sirven para "tomar" o "saltar" 
            // una determinada cantidad de items de nuestra lista. Por ejemplo, si queremos 
            // tomar los primeros 10 clientes:
            clientes.Take(10);
            // si quisieramos tomar los clientes 20, 21.. hasta el 30 podríamos hacer 
            // algo así:
            clientes.Skip(20).Take(10);
            // "salteamos" 20 elementos (para arrancar desde el número 20) y tomamos 10 (20...30)
            // Estos dos métodos son muy utilizados para la paginación!

            // 1) Con los últimos dos métodos que vimos, realizar un paginado en base a las dos 
            // variables siguientes
            int numeroPagina = 5;
            int registrosPorPagina = 1;
            

            // Last: este método es similar a First, con la única diferencia que devuelve
            // el último elemento de la lista. 

            // Distinct: este método funciona igual que en SQL: elimina duplicados.
            clientes.Distinct();

            // ¡ATENCIÓN!: para entender bien el funcionamiento de este método, necesitamos
            // explicar algo antes. En c# tenemos dos estructuras de datos: los struct y las clases
            // generamente uno trabaja con clases, ya que son más perfomantes. ¿Por qué?
            // Porque las clases manejan punteros, y los structs no. Si venis de c/c++ esto te va a sonar
            // familiar, sino, no te preocupes, no es la idea explicar este tipo de conceptos acá.
            // En resumidas cuentas, la igualdad en c# es relativa. ¿Cuál es el consejo entonces?
            // Usa el Distinct SOLAMENTE si tenes listas de datos "nativos", o sea, int, decimal, long,
            // bool, string (ojo mayus y minus acá), etc. No lo uses (o por lo menos la implementación 
            // sin parámetro) para listas de clases (productos, clientes, etc). Existe una implementación
            // en la que podes indicarle cómo comparar dos elementos, pero como no se aplican lambdas
            // no lo vamos a explicar acá.

            // Agregar y eliminar elementos: si bien no implementan lamdas, es importante conocerlos.
            // Para agregar o eliminar dos métodos: Add y Remove.
            // No tienen demasiado misterio, Add recibe un parámetro que será elemento a insertar. 
            // Remove recibe el elemento a eliminar (debemos antes ubicarlo dentro de la lista
            // con alguno de los métodos que vimos antes, como Single)
            // Existen muchos más métodos para agregar o eliminar items de una lista
            // AddRange, RemoveRange, RemoveAt, RemoveAll, Insert, InsertRange..
            // Todos funcionan parecido, no vamos a entrar en detalle. Podes investigarlo y hacer algunas
            // pruebas, son muy fáciles y más aún si vemos las descripción de los métodos que nos
            // muestra el Visual Studio.

            #region Soluciones
            #region (1)
            clientes.Skip(registrosPorPagina * (numeroPagina - 1)).Take(registrosPorPagina).ToList();
            #endregion
            #endregion

        }
    }
}
